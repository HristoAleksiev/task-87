import "../scss/app.scss";
import * as ramda from "ramda";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const arrayToPluck = [
    { name: "John", class: "is-primary" },
    { age: 23, class: "is-warning" },
    { job: "programmer", class: "is-danger" },
  ];

  let pluckedClasses = ramda.pluck("class", arrayToPluck);

  const articles = document.querySelectorAll("article");

  for (let index = 0; index < 3; index++) {
    articles[index].classList.add(pluckedClasses[index]);
  }
});
